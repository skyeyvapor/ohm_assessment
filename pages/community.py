from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User

from models._helpers import *

@app.route('/community', methods=['GET'])
def community():
    """
    :type users: List[result.RowProxy]
    :type phones_list: List[List[result.RowProxy]]
    :type location_list: List[result.RowProxy]
    """

    login_user(User.query.get(1))

    points_and_dollars = current_user.get_points_and_dollars()


    args = {
            'points': points_and_dollars['points'],
            'dollars': points_and_dollars['dollars'],
            'gift_card_eligible': True,

            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
    }

    users = db.engine.execute("SELECT * FROM user ORDER BY create_time DESC LIMIT 5 ").fetchall()

    phones_list = []
    location_list = []

    for user in users:

        # search for user's phone in rel_user_multi
        cmd = "SELECT attribute FROM rel_user_multi WHERE (user_id=%s)" % user.user_id

        # will list all
        phones = db.engine.execute(cmd).fetchall()
        
        #if phones:
        #    print type(phones[0])  # <class 'sqlalchemy.engine.result.RowProxy'>

        phones_list.append(phones)

        

        # search for user's location in rel_user
        cmd = "SELECT attribute FROM rel_user WHERE (user_id=%s)" % user.user_id
        location = db.engine.execute(cmd).fetchone()  # only one location     
        location_list.append(location)


    
    return render_template("community.html", users=users, phones_list=phones_list, location_list=location_list)

