"""migration to increase the point_balance for user 2 to 1000, and the tier for user 3 to Bronze

Revision ID: 3a1d8a9dcf28
Revises: 00000000
Create Date: 2017-10-28 22:25:55.427631

"""

# revision identifiers, used by Alembic.
revision = '3a1d8a9dcf28'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("UPDATE user SET point_balance=1000 WHERE user_id=2")
    op.execute("UPDATE user SET tier='Bronze' WHERE user_id=3")


def downgrade():
    pass
